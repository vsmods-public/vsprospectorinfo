# VsProspectorInfo
A clientside only mod to save trees by not having to write down the values of your prospecting.

See full description here: https://mods.vintagestory.at/show/mod/91
