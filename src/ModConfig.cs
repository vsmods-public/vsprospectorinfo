﻿using Foundation.ModConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vsprospectorinfo.src.Models;

namespace vsprospectorinfo.src
{
    public class ModConfig : ModConfigBase
    {
        public override string ModCode
        {
            get
            {
                return "vsprospectorinfo";
            }
        }

        public bool RenderTexturesOnMap { get; set; } = false;
        public ColorWithAlpha TextureColor { get; set; } = new ColorWithAlpha(150, 125, 150, 128);
        public ColorWithAlpha BorderColor { get; set; } = new ColorWithAlpha(0, 0, 0, 200);
        public int BorderThickness { get; set; } = 1;
        public bool RenderBorder { get; set; } = true;
        public bool AutoToggle { get; set; } = true;
    }
}
